## Contributing new rules

To test a new rule you're writing, Semgrep has a really useful "playground" over at https://semgrep.dev/editor. Put your rule there with a bit of code you're trying to match (or not match) and have fun. Of course don't put active GitLab vulnerabilities in there.

## Style guide
1. Make sure the rule ID begins with `glappsec_`.
1. For rules that enforce [secure coding guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html), the following fields must be included in the yaml:

  ```yml
  message: > 
    Detection reasoning. Link to the secure coding guideline.
  metadata:
    vulnerability_class: "Class of vulnerability or title of the offended secure coding guideline"
    confidence: LOW/MEDIUM/HIGH
  severity: INFO/WARNING/ERROR
  ```

  You can find an example of a compliant rule [here](https://gitlab.com/gitlab-com/gl-security/product-security/appsec/sast-custom-rules/-/blob/025b2dbe6c8434962c491a9db3e1640d4fc48ec4/secure-coding-guidelines/ruby/ruby_command_injection.yml).

### Licensing
If you copy any new rules from the [Semgrep registry](https://semgrep.dev/r), they should include the license at the top of the rule, like so:
```
// License: Semgrep Rules License v. 1.0 (https://semgrep.dev/legal/rules-license/)
// Source: <link to the original rule>
```

You can find an example MR with the required licensing contents [here](https://gitlab.com/gitlab-com/gl-security/product-security/appsec/sast-custom-rules/-/merge_requests/31/diffs). 

Any rules that have been copied from the registry, but modified in any way should additionally be prefixed as below:
```
// Modification. Copyright owner: [Date of Modification] - present GitLab, Inc.  
```

### Tests

Semgrep has a syntax to write unit tests which you can read about [here](https://semgrep.dev/docs/writing-rules/testing-rules/). You can see the [`appsec-pings/html_safe.rb`](./appsec-pings/html_safe.rb) file to see it in action. Semgrep requires that for a rule named `rule_name.yml`, the test file must be named `rule_name.ext` where `ext` is the normal extension for the programming language you're testing.

### Trying on the `gitlab-org/gitlab` project

To test at scale you can write your rule to a local file and then run [the command from our SAST pipelines](https://gitlab.com/gitlab-org/gitlab/-/blob/32b061f901d7a07f5e3aeffc04dbc4cd663ef9cd/.gitlab/ci/static-analysis.gitlab-ci.yml#L200-L202).

```sh
semgrep ci --gitlab-sast --metrics off --config "${CI_BUILDS_DIR}/sast-custom-rules" \
        --include app --include lib --include workhorse \
        --exclude '*_test.go' --exclude spec --exclude qa --exclude tooling
```

There are 2 modifications to make:

1. Change `--config "${CI_BUILDS_DIR}/sast-custom-rules"` to `--config NAME_OF_YOUR_FILE.YML`
1. (Optional) Remove the `--gitlab-sast` flag to have human-readable output instead of JSON

Which results in something that looks like

```sh
semgrep ci --gitlab-sast --metrics off --config rules.yml --include app --include lib --include workhorse --exclude '*_test.go' --exclude spec --exclude qa
```

You can then use the output of this to see if you get the expected results on the GitLab code base.

It can be useful to run the command once using the old rule and output to a file to have a baseline and then do it again with the new rule and diff both outputs.

For example the output of `diff --color baseline.txt new.txt` for [this MR](https://gitlab.com/gitlab-com/gl-security/product-security/appsec/sast-custom-rules/-/merge_requests/17) was

```diff
4c4
< │ 207 Blocking Code Findings │
---
> │ 200 Blocking Code Findings │
427,455d426
<                                                 
<     app/helpers/notify_helper.rb 
<        glappsec_dangerous_html_safe                                                         
<           `html_safe` usage is risky and frequently leads to XSS. Please review carefully to make sure
<           that no unsanitized user input can reach link_to(merge_request.to_reference,                
<           merge_request_url(merge_request), style: "font-weight: 600;color:#3777b0;text-              
<           decoration:none").                                                                          
<                                                                                                       
<            28┆  ... link_to(merge_request.to_reference, merge_request_url(merge_request), style: "font-weight:   
<   600;color:#3777b0;text-decoration:none").html_safe, ...                                                         
<             [shortened a long line from output, adjust with --max-chars-per-line]
<             ⋮┆----------------------------------------
<        glappsec_dangerous_html_safe                                                         
<           `html_safe` usage is risky and frequently leads to XSS. Please review carefully to make sure
<           that no unsanitized user input can reach content_tag(:img, nil, height: "24", src:          
<           avatar_icon_for_user(reviewer, 24, only_path: false), style: "border-radius:12px;margin:-7px
<           0 -7px 3px;", width: "24", alt: "Avatar", class: "avatar").                                 
<                                                                                                       
<            30┆  ... content_tag(:img, nil, height: "24", src: avatar_icon_for_user(reviewer, 24, only_path:      
<   false), style: "border-radius:12px;margin:-7px 0 -7px 3px;", wi ...                                                
<             [shortened a long line from output, adjust with --max-chars-per-line]
<             ⋮┆----------------------------------------
<        glappsec_dangerous_html_safe                                                         
<           `html_safe` usage is risky and frequently leads to XSS. Please review carefully to make sure
<           that no unsanitized user input can reach link_to(reviewer.name, user_url(reviewer), style:  
<           "color:#333333;text-decoration:none;", class: "muted").                                     
<                                                                                                       
<            31┆ reviewer_link: link_to(reviewer.name, user_url(reviewer), style:                                  
<   "color:#333333;text-decoration:none;", class: "muted").html_safe                                                   
473,487d443
<           that no unsanitized user input can reach link_to(author_html, user_path(author), class:     
<           inject_classes, data: data_attrs).                                                          
<                                                                                                       
<            88┆ link_to(author_html, user_path(author), class: inject_classes, data: data_attrs).html_safe
<             ⋮┆----------------------------------------
<        glappsec_dangerous_html_safe                                                         
<           `html_safe` usage is risky and frequently leads to XSS. Please review carefully to make sure
<           that no unsanitized user input can reach link_to(author_html, user_path(author), class:     
<           inject_classes, title: title, data: { container: 'body', qa_selector: 'assignee_link' }).   
<                                                                                                       
<            91┆ link_to(author_html, user_path(author), class: inject_classes, title: title, data: { container:   
<   'body', qa_selector: 'assignee_link' }).html_safe                                                                  
<             ⋮┆----------------------------------------
<        glappsec_dangerous_html_safe                                                         
<           `html_safe` usage is risky and frequently leads to XSS. Please review carefully to make sure
537,552d492
<             ⋮┆----------------------------------------
<        glappsec_dangerous_html_safe                                                         
<           `html_safe` usage is risky and frequently leads to XSS. Please review carefully to make sure
<           that no unsanitized user input can reach link_to(project.full_name, project_path(project),  
<           target: '_blank', rel: 'noopener noreferrer').                                              
<                                                                                                       
<           140┆ project: link_to(project.full_name, project_path(project), target: '_blank', rel: 'noopener       
<   noreferrer').html_safe                                                                                             
<             ⋮┆----------------------------------------
<        glappsec_dangerous_html_safe                                                         
<           `html_safe` usage is risky and frequently leads to XSS. Please review carefully to make sure
<           that no unsanitized user input can reach link_to(group.full_name, group_path(group), target:
<           '_blank', rel: 'noopener noreferrer').                                                      
<                                                                                                       
<           144┆ group: link_to(group.full_name, group_path(group), target: '_blank', rel: 'noopener               
<   noreferrer').html_safe                                  
```

Showing that the results were indeed as expected with all the `link_to(...).htmL_safe` and `content_tag(...).html_safe` false positives being removed.
