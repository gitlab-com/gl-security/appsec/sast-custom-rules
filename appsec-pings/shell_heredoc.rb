# ruleid: glappsec_shell_heredoc
container_args = <<~SH.chomp
  if [ ! -d '#{clone_dir}' ];
  then
    git clone --branch #{Shellwords.shellescape(project_ref)} #{Shellwords.shellescape(project_url)} #{Shellwords.shellescape(clone_dir)};
  fi
SH

# ruleid: glappsec_shell_heredoc
container_args = <<~SHELL.chomp
  if [ ! -d '#{clone_dir}' ];
  then
    git clone --branch #{Shellwords.shellescape(project_ref)} #{Shellwords.shellescape(project_url)} #{Shellwords.shellescape(clone_dir)};
  fi
SHELL

# ruleid: glappsec_shell_heredoc
container_args = <<~BASH
  if [ ! -d '#{clone_dir}' ];
  then
    git clone --branch #{Shellwords.shellescape(project_ref)} #{Shellwords.shellescape(project_url)} #{Shellwords.shellescape(clone_dir)};
  fi
BASH

# ruleid: glappsec_shell_heredoc
container_args = <<-SH.chomp
  if [ ! -d '#{clone_dir}' ];
  then
    git clone --branch #{Shellwords.shellescape(project_ref)} #{Shellwords.shellescape(project_url)} #{Shellwords.shellescape(clone_dir)};
  fi
SH

# ok: glappsec_shell_heredoc
container_args = <<-'SH'
  echo "no interpretation this would output #{the literal characters}"
SH

# ok: glappsec_shell_heredoc
something = <<~TXT
BLABLA
>>