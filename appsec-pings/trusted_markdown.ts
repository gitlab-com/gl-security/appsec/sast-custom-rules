const x = new vscode.MarkdownString(undefined, true)
//ruleid: glappsec_ts-markdown-trusted
x.isTrusted = true

//ok: glappsec_ts-markdown-trusted
x.isTrusted = false

function test() {
    const markdown: vscode.MarkdownString = new vscode.MarkdownString(undefined, true);

    //ruleid: glappsec_ts-markdown-trusted
    markdown.isTrusted = true;
}
