if (!wrapperElement.classList.contains('lazy-alert-shown')) {
    // ruleid: glappsec_eslint-disable-next-line-no-unsanitized-property_disable_gitlabsecurity
    // eslint-disable-next-line no-unsanitized/property
    wrapperElement.innerHTML = html;
    wrapperElement.append(codeElement);
    wrapperElement.classList.add('lazy-alert-shown');
}

