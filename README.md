# SAST Custom Rules

Centralized repository for SAST custom rules.

These rules detect code that is likely to lead to a vulnerability and where an AppSec review would be important. They also enforce secure coding standards.

## FAQ

### The bot commented on my MR, what do I do with this? Is there anything wrong?

The bot uses custom SAST rules to look for code patterns that led to vulnerabilities before. We're trying to keep the noise level as low as it can be, but it's possible that there are no problems with your MR! Based on the findings enumerated in the bot's comment and the customized message for each finding, validate if they are true positives. If you would like help doing that do not hesitate to involve `@gitlab-com/gl-security/product-security/appsec`, they have already been pinged anyway!

If the finding was indeed a true positive, the bot will remove the comment once the issue has been resolved. If it is a false positive then don't worry, the comment is not blocking and you can proceed as normal.

### I have feedback, where do I give it?

We have created [issue labels in `gitlab-org`](https://gitlab.com/groups/gitlab-org/-/labels?search=appsec-sast) that you can use:

- `appsec-sast::commented` is added automatically when the bot comments
- `appsec-sast::helpful` should be added when the custom rules helped address a true positive, or was otherwise helpful/informative
- `appsec-sast::unhelpful` can be added by any team member to signal a low quality or unhelpful result

For specific feedback, open an issue in this project and ping `@gitlab-com/gl-security/product-security/appsec`. Be aware that this is a public project so please open a confidential issue if you're going to discuss about existing vulnerabilities.

### How to add new rules?

See [CONTRIBUTING.md](./CONTRIBUTING.md)
