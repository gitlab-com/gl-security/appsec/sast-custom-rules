func PathTest(evilPath string) {
    goodprefix := "/home/directory/"

    //ok: glappsec_path-traversal-go
	result3 := path.Join(goodprefix, path.Join("/", path.Clean(evilPath)))
}

func PathTest2(evilPath, test string) {
    //ruleid: glappsec_path-traversal-go
	result1 := path.Join("goodprefix", evilPath)
   
    os.Open(result1)

    //ok: glappsec_path-traversal-go
	result3 := os.Open(path.Join(goodprefix, path.Join("/", path.Clean(evilPath))))
}