package main

import (
    zip "archive/zip"
)

// unzip INSECURELY extracts source zip file to destination.
func unzip(src, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
	  return err
	}
	defer r.Close()
  
	os.MkdirAll(dest, 0750)
  
	for _, f := range r.File {
	  if f.FileInfo().IsDir() { // Skip directories in this example for simplicity.
		continue
	  }
  
	  rc, err := f.Open()
	  if err != nil {
		return err
	  }
	  defer rc.Close()
  
	  path := filepath.Join(dest, f.Name)

	  //ruleid: glappsec_insecure-archive-go
	  os.MkdirAll(filepath.Dir(path), f.Mode())
      //ruleid: glappsec_insecure-archive-go
	  f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
	  if err != nil {
		return err
	  }
	  defer f.Close()
  
	  if _, err := io.Copy(f, rc); err != nil {
		return err
	  }
	}
  
	return nil
  }

// unzip extracts source zip file to destination with protection against Zip Slip attacks.
func unzip(src, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
	  return err
	}
	defer r.Close()
  
	os.MkdirAll(dest, 0750)
  
	for _, f := range r.File {
	  if f.FileInfo().IsDir() { // Skip directories in this example for simplicity.
		continue
	  }
  
	  rc, err := f.Open()
	  if err != nil {
		return err
	  }
	  defer rc.Close()
  
	  path := filepath.Join(dest, f.Name)
  
	  if !strings.HasPrefix(path, filepath.Clean(dest) + string(os.PathSeparator)) {
		return fmt.Errorf("illegal file path: %s", path)
	  }
	
	  // ok: glappsec_insecure-archive-go
	  os.MkdirAll(filepath.Dir(path), f.Mode())
	  f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
	  if err != nil {
		return err
	  }
	  defer f.Close()
  
	  if _, err := io.Copy(f, rc); err != nil {
		return err
	  }
	}
  
	return nil
  }