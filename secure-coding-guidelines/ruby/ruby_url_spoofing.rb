def some_rails_controller
    #ruleid: glappsec_url-spoofing
    link_to foo_social_url(params[:url]), title: "Foo Social" do
      sprite_icon('question-o')
    end
  end
  
  def some_rails_controller
    #ok: glappsec_url-spoofing
    link_to external_redirect_path(params[:url]), title: "Foo" do
      sprite_icon('question-o')
    end
  end