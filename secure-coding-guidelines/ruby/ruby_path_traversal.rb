def readFile(x)
  #ruleid: glappsec_path-traversal
  File.open(x)
  #ruleid: glappsec_path-traversal
  IO.read(x)
  #ruleid: glappsec_path-traversal
  IO.readlines(x)
  #ruleid: glappsec_path-traversal
  send_file x
end

def secureReadFile(x)
  Gitlab::PathTraversal.check_path_traversal!(x)
  #ok: glappsec_path-traversal
  File.write(x)
end

def secureReadFile(x)
  Gitlab::PathTraversal.check_allowed_absolute_path!(x)
  random()
  #ruleid: glappsec_path-traversal
  IO.read(x)
end

def secureReadFile(x)
  Gitlab::PathTraversal.check_allowed_absolute_path_and_path_traversal!(x)
  #ok: glappsec_path-traversal
  send_file x
end

#ruleid: glappsec_path-traversal
File.read(params[:filename])

Gitlab::PathTraversal.check_allowed_absolute_path!(params[:filename])
#ruleid: glappsec_path-traversal
File.write(params[:filename])

Gitlab::PathTraversal.check_path_traversal!(params[:filename])
#ok: glappsec_path-traversal
IO.read(params[:filename])

Gitlab::PathTraversal.check_allowed_absolute_path_and_path_traversal!(params[:filename])
#ok: glappsec_path-traversal
IO.readlines(params[:filename])

Gitlab::PathTraversal.check_allowed_absolute_path_and_path_traversal!(params[:filename])
#ok: glappsec_path-traversal
send_file params[:filename]
