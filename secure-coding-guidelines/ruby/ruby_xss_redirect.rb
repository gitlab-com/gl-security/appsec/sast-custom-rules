# ruleid: glappsec_dangerous_redirect
redirect_to(params[:t])

# ruleid: glappsec_dangerous_redirect
redirect_to request.referer

# ok: glappsec_dangerous_redirect
redirect_to user_profile_path(current_user)