def read(url)
    x = Addressable::URI.parse(url)
# ruleid: glappsec_insecure-url-parsing-2
    Net::HTTP.get(x)

    x = URI.parse(url)
# ruleid: glappsec_insecure-url-parsing-2
    HTTParty.post(x)
end

def read(url)
    x = Addressable::URI.parse(url)
# ok: glappsec_insecure-url-parsing-2
    Gitlab::HTTP.get(x)
end