options = {  
        allow_local_requests: true,  
        headers: cube_security_headers  
      }

begin
  #ruleid: glappsec_insecure-url-construction-2
  Gitlab::HTTP.get(cube_server_url(params[:path]), options)  
rescue Gitlab::Json.parser_error, *Gitlab::HTTP::HTTP_ERRORS => e  
  return ServiceResponse.error(message: e.message, reason: :bad_gateway)  
end

options = {  
        allow_local_requests: false,  
      }

begin
  #ok: glappsec_insecure-url-construction-2
  Gitlab::HTTP.get(cube_server_url(params[:path]), options)  
rescue Gitlab::Json.parser_error, *Gitlab::HTTP::HTTP_ERRORS => e  
  return ServiceResponse.error(message: e.message, reason: :bad_gateway)  
end
