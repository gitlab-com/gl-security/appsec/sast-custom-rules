require 'open3'

def test_params()
  user_input = params['some_key']
# ruleid: glappsec_dangerous-exec
  exec("ls -lah #{user_input}")

# ruleid: glappsec_dangerous-exec
  Process.spawn([user_input, "smth"])

# ruleid: glappsec_dangerous-exec
  output = exec(["sh", "-c", user_input])

# ruleid: glappsec_dangerous-exec
  pid = spawn(["bash", user_input])

# ruleid: glappsec_dangerous-exec
  Gitlab::Popen.popen(["bash", user_input])

# ruleid: glappsec_dangerous-exec
  Gitlab::Popen.popen_with_detail(["bash", user_input])

  commands = "ls -lah /raz/dva"
# ok: glappsec_dangerous-exec
  system(commands)

  cmd_name = "sh"
# ok: glappsec_dangerous-exec
  Process.exec([cmd_name, "ls", "-la"])
# ok: glappsec_dangerous-exec
  Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
# ok: glappsec_dangerous-exec
  system("ls -lah /tmp")
# ok: glappsec_dangerous-exec
  exec(["ls", "-lah", "/tmp"])
# ok: glappsec_dangerous-exec
  Gitlab::Popen.popen(["ls", "-lah", "/tmp"])
# ok: glappsec_dangerous-exec
  Gitlab::Popen.pope_with_detail(["ls", "-lah", "/tmp"])
end

def test_calls(user_input)
  # ruleid: glappsec_dangerous-exec
    exec("ls -lah #{user_input}")
  
  # ruleid: glappsec_dangerous-exec
    Process.spawn([user_input, "smth"])
  
  # ruleid: glappsec_dangerous-exec
    output = exec(["sh", "-c", user_input])
  
  # ruleid: glappsec_dangerous-exec
    pid = spawn(["bash", user_input])

  # ruleid: glappsec_dangerous-exec
    Gitlab::Popen.popen(["bash", user_input])

  # ruleid: glappsec_dangerous-exec
    Gitlab::Popen.popen_with_detail(["bash", user_input])
  
  commands = "ls -lah /raz/dva"
  # ok: glappsec_dangerous-exec
    system(commands)
  
    cmd_name = "sh"
  # ok: glappsec_dangerous-exec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: glappsec_dangerous-exec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: glappsec_dangerous-exec
    system("ls -lah /tmp")
  # ok: glappsec_dangerous-exec
    exec(["ls", "-lah", "/tmp"])
  # ok: glappsec_dangerous-exec
    Gitlab::Popen.popen(["ls", "-lah", "/tmp"])
  # ok: glappsec_dangerous-exec
  Gitlab::Popen.pope_with_detail(["ls", "-lah", "/tmp"])
  end

  def test_cookies()
    user_input = cookies['some_cookie']
    # ruleid: glappsec_dangerous-exec
      exec("ls -lah #{user_input}")
    
    # ruleid: glappsec_dangerous-exec
      Process.spawn([user_input, "smth"])
    
    # ruleid: glappsec_dangerous-exec
      output = exec(["sh", "-c", user_input])
    
    # ruleid: glappsec_dangerous-exec
      pid = spawn(["bash", user_input])

    # ruleid: glappsec_dangerous-exec
      Gitlab::Popen.popen(["bash", user_input])

    # ruleid: glappsec_dangerous-exec
      Gitlab::Popen.popen_with_detail(["bash", user_input])
    
      commands = "ls -lah /raz/dva"
    # ok: glappsec_dangerous-exec
      system(commands)
    
      cmd_name = "sh"
    # ok: glappsec_dangerous-exec
      Process.exec([cmd_name, "ls", "-la"])
    # ok: glappsec_dangerous-exec
      Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
    # ok: glappsec_dangerous-exec
      system("ls -lah /tmp")
    # ok: glappsec_dangerous-exec
      exec(["ls", "-lah", "/tmp"])
    # ok: glappsec_dangerous-exec
      Gitlab::Popen.popen(["ls", "-lah", "/tmp"])
     # ok: glappsec_dangerous-exec
     Gitlab::Popen.pope_with_detail(["ls", "-lah", "/tmp"])
    end