#ruleid: glappsec_insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", allow_localhost: true)

#ok: glappsec_insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", allow_localhost: false, schemes: ['http']
)

#ruleid: glappsec_insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", allow_localhost: false) #no schemes

#ruleid: glappsec_insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", dns_rebind_protection: false, allow_localhost: false)

#ruleid: glappsec_insecure-url-construction
Gitlab::UrlBlocker.validate!("https://gitlab.com", allow_localhost: true)