def bad_deserialization
    o = Klass.new("hello\n")
    data = params['data']
    # ruleid: glappsec_bad-deserialization
    obj = Marshal.load(data)

    o = Klass.new("hello\n")
    data = YAML.dump(o)
    # ok: glappsec_bad-deserialization
    obj = YAML.load(data)

    o = Klass.new(params['hello'])
    data = CSV.dump(o)
    # ruleid: glappsec_bad-deserialization
    obj = CSV.load(data)

    o = Klass.new("hello\n")
    data = cookies['some_field']
    # ruleid: glappsec_bad-deserialization
    obj = Oj.object_load(data)
    # ruleid: glappsec_bad-deserialization
    obj = Oj.load(data)
   # ok: glappsec_bad-deserialization
   obj = Oj.load(data,options=some_safe_options)
 end

 def ok_deserialization
    o = Klass.new("hello\n")
    data = YAML.dump(o)
    # ok: glappsec_bad-deserialization
    obj = YAML.load(data, safe: true)

    filename = File.read("test.txt")
    data = YAML.dump(filename)
    # ok: glappsec_bad-deserialization
    YAML.load(filename)

    # ok: glappsec_bad-deserialization
    YAML.load(File.read("test.txt"))
 end
