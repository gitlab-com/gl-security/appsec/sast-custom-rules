def some_rails_controller
    foo = params[:some_regex]
    #ruleid: glappsec_redos_1
    Regexp.new(foo).match("some_string")
end
  
def some_rails_controller
    bar = ENV['someEnvVar']
    #ok: glappsec_redos_1
    Regexp.new(bar).match("some_string")
end
  
def use_params_in_regex
    #ruleid: glappsec_redos_1
    @x = something.match /#{params[:x]}/
end
  
def regex_on_params
    #ok: glappsec_redos_1
    @x = params[:x].match /foo/
end