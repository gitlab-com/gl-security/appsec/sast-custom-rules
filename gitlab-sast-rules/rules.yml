# Rule IDs should start with the `glappsec_` prefix
rules:
- id: glappsec_dangerous_untar
  patterns:
    - pattern: $UNTAR(...)
    - metavariable-regex:
        metavariable: $UNTAR
        regex: untar_\w*
    - pattern-inside: |
        include Gitlab::ImportExport::CommandLineUtil
        
        ...
  message: "`Gitlab::ImportExport::CommandLineUtil.$UNTAR` method used, verify if a symlink attack is possible"
  languages: [ruby]
  severity: WARNING
  metadata:
    # https://gitlab.com/gitlab-org/gitlab/-/issues/371399#note_1125343804
    cve: CVE-b26b63bb8cb9d52c09689cb6cb313f00
    references:
      - https://gitlab.com/gitlab-org/gitlab/-/issues/349524
      - https://gitlab.com/gitlab-org/gitlab-foss/-/issues/23822
    category: security
    security-severity: "HIGH"
- id: glappsec_dangerous_string_interpolation
  mode: taint
  pattern-sanitizers:
    - pattern-either:
      - pattern: escape(...)
      - pattern: sanitize(...)
      - pattern: $JQUERY.attr('...', ...)
      - pattern: lodashEscape(...)
  pattern-sinks:
    - patterns:
      - pattern: $SINK
      - pattern: "`...${$SINK}...`"
      - pattern-inside: "`$HTML${...}...`"
      - metavariable-regex:
          metavariable: $HTML
          regex: ^\s*<
      - metavariable-pattern:
          metavariable: $SINK
          patterns:
            - pattern-not-regex: |
                (?x)(
                    # sink ends with...
                    .*
                    (
                        [Uu]rl # URL variables create a lot of noise
                      |
                        [Pp]ath # Same with paths
                      |
                        [_\.]id # IDs are numeric
                      |
                        [a-z]Id # Same as above with camelCase
                    )
                  |
                    # or sink starts with...
                    (
                        is[A-Z_] # a isSomething or is_something variable is boolean
                      |
                        sprintf\( # i18n
                      |
                        [A-Z]{2,} # Constant
                    )
                    .*
                  |
                    # `'foo' in bar` format
                    (
                        ('[^']+'|"[^"]+")\sin\s[a-zA-Z_.]+
                    )
                )
  pattern-sources:
    - pattern-either:
      - patterns:
          - pattern-either:
            - pattern-inside: function ... (..., $PARAM, ...) { ... }
            - pattern-inside: function ... ({..., $PARAM, ...}) { ... }
          - pattern: $PARAM
  languages:
    - javascript
    - typescript
  metadata:
    # https://gitlab.com/gitlab-org/gitlab/-/issues/371399#note_1125343804
    cve: CVE-ea3b7a7a503dc304b07b83024dfb008c
    references:
      - https://gitlab.com/gitlab-org/gitlab/-/issues/345657
      - https://gitlab.com/gitlab-org/gitlab/-/issues/363293
      - https://gitlab.com/gitlab-org/gitlab/-/issues/364164
    category: security
  message: "Verify if `$SINK` (coming from parameter `$PARAM`) is user-controlled and needs to be escaped before being used in a template string to avoid XSS."
  severity: ERROR
